
import './App.css';
import React, { Fragment } from 'react';
import { useForm } from "react-hook-form";
import './css/bootstrap.css';
import './css/miestilo.css';


const Login = () =>{
const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const onSubmit = data => console.log(data);

  return(
      <div className='body'> 
    <div className="container">
    <div className='d-flex justify-content-center h-100'>
        <div className="card">
            <div className="card-header">
                <div className="text-white h2">Inicio Sesión</div>
            </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-white"> Clave de Active Directory:</div>
                    <div className="input-group form-group">
                        <input type="text" className="form-control" name="activedirectory" 
                           {...register("activedirectory", { required: true })}></input>
                         
                    </div>
                    {errors.activedirectory && <span class="text-danger form-width-600 h5">Este campo es requerido</span>}
                    <div className="text-white"> Contraseña:</div>
                    <div className="input-group form-group">
                        <div className="input-group-prepend">
                        </div>
                        <input type="password" className="form-control" name="contraseña"
                        {...register("contraseña", { required: true })}></input>
                    </div>
                    {errors.contraseña && <span class="text-danger form-width-600 h5">Este campo es requerido</span>}
                    <br></br>
                    <div className="row align-items-center remember h6 text-white">
                    <input type="checkbox"></input> recuerdame 
                    </div>
                    <br></br>
                    <div className="form-group">
                        <input type="submit" value="Login" className="btn float-right login_btn"></input>
                    </div>
                </form>
                
            <div className="card-footer">
                <div className="d-flex justify-content-center">
                    <a href="www.google.cl">¿Oklvido su contraseña?</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
  )
}


function App() {
  return (
    <React.Fragment>
      <Login></Login>
    </React.Fragment>
  );
}

export default App;
